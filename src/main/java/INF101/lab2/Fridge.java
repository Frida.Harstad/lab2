package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	
	int maxSize = 20;
	List <FridgeItem> itemList = new ArrayList<FridgeItem>();
	
	
	// total capacity
	public int totalSize() {
		return maxSize;
	}
	
	
	// current items
	public int nItemsInFridge() {
		return this.itemList.size();
	}

	
	// add items if fridge has space
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge() < maxSize) {
			itemList.add(item);
			return true;
		} 
		return false;
	}

	
	// take out an item from fridge
	public void takeOut(FridgeItem item) {
		if (itemList.contains(item)) {
			itemList.remove(item);
		} else {
			throw new NoSuchElementException();
		}
		
	}
	
	
	// remove all items
	public void emptyFridge() {
		itemList.clear();
		
	}

	
	// detect and remove expired items
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expiredItemsList = new ArrayList<>();
		
		for (FridgeItem item : itemList) {
			if (item.hasExpired()) {
				expiredItemsList.add(item);
			}
		}
		for (FridgeItem item : expiredItemsList) {
			itemList.remove(item);
		}
		return expiredItemsList;
	}

}
